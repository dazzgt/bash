﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SQLite;
using System.Timers;
using System.Windows;
using Delay;

namespace Bot.VK
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow 
    {

        //const string appID = "3991661";
        //const string scope = "2015231,offline";
        private const string grpIdBash = "-43196709";
        private const string grpZadolbali = "-71820136";
        private const string grpIdIT = "-61326931";

        private const string token =
            "fb5e8862d3eebec45fa75125b3266ebb51688f088d7b941656ae3f63ddc1df811da181385d5b07b97a983";

        public Bash bash = new Bash();
        public Stories it = new Stories("http://ithappens.me/");
        public Stories zad = new Stories("http://zadolba.li/");
        private string lastBest = "";

        private SQLiteConnection con;
        private VkApi api = new VkApi();
        private Timer Routine;
        private Timer Best;
        private bool _best = false;
        private bool _routine = false;
        public MainWindow()
        {
            InitializeComponent();

            GetSettings();

            Routine = new Timer();
            Routine.Elapsed += Routine_Elapsed;
            Routine.Interval = 10000;
            Routine.Start();

            Best = new Timer();
            Best.Elapsed += Best_Elapsed;
            Best.Interval = 50000;
            Best.Start();

            DataContext = this;
            MinimizeToTray.Enable(this);

        }


        private int lastQuote;
        private string lastComics;
        private int lastItStory;
        private int lastZadolbali;

        ObservableCollection<Ac> _comics = new ObservableCollection<Ac>();

        public ObservableCollection<Ac> Coms
        {
            get { return _comics; }
        }
        void Best_Elapsed(object sender, ElapsedEventArgs args)
        {
            if (DateTime.Now.Minute != 0)
            {
                _best = false;
                return;
            }
            if (_best)
                return;
            Best.Stop();

            try
            {
                Quote q = GetBestQuote();
                if (q != null)
                {
                    api.Post(grpIdBash, "#bash #баш #best@bashquote " + q.GetText(), token);
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }

            try
            {
                Comics c = bash.GetComics(lastComics);
                if (c != null)
                {
                    var list = api.GetAlbums(grpIdBash, token);
                    var album = list.Find(x => x.title.Contains(c.Date.Remove(4)));
                    if (album != null)
                    {
                        int id = album.id;
                        if (api.SaveAlbumPhoto(grpIdBash, id.ToString(), token, c.Path, c.Text))
                        {
                            lastComics = c.Date;
                            api.SaveWallPhoto(grpIdBash, token, c);
                        }
                    }
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }

            try
            {
                foreach (var a in _comics)
                    UpdateAComics(a);
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            Best.Start();
        }
        void Routine_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (DateTime.Now.Minute % 5 != 0)
            {
                _routine = false;
                if (!String.IsNullOrEmpty(lastBest))
                {
                    con = new SQLiteConnection("Data Source=filename.db; Version=3;");
                    SQLiteCommand cmd = new SQLiteCommand(con);
                    try
                    {
                        con.Open();
                        cmd.CommandText = lastBest;
                        cmd.ExecuteReader();
                        con.Close();
                        lastBest = "";
                    }
                    catch { con.Close();}
                }
                return;
            }
            if (_routine) return;
            Routine.Stop();
            try
            {
                bash.GetNewQuotes(lastQuote);
                foreach (var a in bash.NewQuotes)
                {
                    if (!api.Post(grpIdBash, "#bash #баш #new@bashquote " + a.GetText(), token))
                        break;
                    lastQuote = Convert.ToInt32(a.Number.Replace("#", ""));
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }

            try
            {
                it.GetNewStories(lastItStory);
                foreach (var a in it.NewStories)
                {
                    if (!api.Post(grpIdIT, "#ithappens " + a.GetText(), token))
                        break;
                    lastItStory = Convert.ToInt32(a.Number.Replace("#", ""));
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }

            try
            {
                zad.GetNewStories(lastZadolbali);
                foreach (var a in zad.NewStories)
                {
                    if (!api.Post(grpZadolbali, "#zadolbali #задолбали\r\n" + a.GetText(), token))
                        break;
                    lastZadolbali = Convert.ToInt32(a.Number.Replace("#", ""));
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            SaveSettings();
            Routine.Start();

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Quote q = GetBestQuote();
                if (q != null)
                {
                    api.Post(grpIdBash, "#bash #баш #best@bashquote " + q.GetText(), token);
                }
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }
        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            Ac ac = new AddComics().ShowModal();
            _comics.Add(ac);
            try
            {
                SQLiteConnection con = new SQLiteConnection("Data Source=filename.db; Version=3;");
                SQLiteCommand cmd = new SQLiteCommand(con);
                con.Open();
                cmd.CommandText = "INSERT INTO AComics VALUES('" + ac.Num + "','" + ac.Name + "','" + ac.Album + "');";
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }
        private void ButtonComics_Click(object sender, RoutedEventArgs e)
        {
            Comics c = bash.GetComics(lastComics);
            if (c != null)
            {
                var list = api.GetAlbums(grpIdBash, token);
                var album = list.Find(x => x.title.Contains(c.Date.Remove(4)));
                if (album != null)
                {
                    int id = album.id;
                    if (api.SaveAlbumPhoto(grpIdBash, id.ToString(), token, c.Path, c.Text))
                    {
                        lastComics = c.Date;
                        api.SaveWallPhoto(grpIdBash, token, c);
                    }
                }
            }
        }

        private void ButtonTest_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (var a in _comics)
                    UpdateAComics(a);
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
        }

        private void ButtonUpdateDB_Click(object sender, RoutedEventArgs e)
        {
            UpdateBestQuotes();

        }
        public void UpdateAComics(Ac ac)
        {
            Comics c;
            if (ac.Num == "0")
                c = AComics.GetComics("1", ac.Name, false);
            else
                c = AComics.GetComics(ac.Num, ac.Name);
            List<Album> list = api.GetAlbums(grpIdBash, token);
            string last = "";
            if (list == null)
                return;
            Album al = list.Find(x => x.title.Contains(ac.Album));
            if (al == null) return;
            while (c != null)
            {
                if (!api.SaveAlbumPhoto(grpIdBash, al.id.ToString(), token, c.Path))
                    continue;
                last = c.Date;
                c = AComics.GetComics(c.Date, ac.Name);
            }
            if (last != "")
            {
                ac.Num = last;
                SaveLastComics(ac);
            }
        }
        private void SaveLastComics(Ac ac)
        {
            try
            {
                SQLiteConnection con = new SQLiteConnection("Data Source=filename.db; Version=3;");
                SQLiteCommand cmd = new SQLiteCommand(con);
                con.Open();
                cmd.CommandText = "UPDATE AComics SET Num=" + ac.Num + " WHERE Name='" + ac.Name + "';";
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch { }
        }
        public bool UpdateBestQuotes()
        {
            try
            {
                SQLiteConnection con = new SQLiteConnection("Data Source=filename.db; Version=3;");
                SQLiteCommand cmd = new SQLiteCommand(con);
                con.Open();
                cmd.CommandText = "DELETE FROM BestQuotes;";
                cmd.ExecuteNonQuery();
                List<Quote> res = bash.GetBestQuotes(100);
                foreach (var a in res)
                {
                    cmd.CommandText = "INSERT INTO BestQuotes VALUES('" + a.Text.Replace("'", "&prime;") + "','" + a.Number + "','" + a.Date + "');";
                    cmd.ExecuteNonQuery();
                }
                con.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public Quote GetBestQuote()
        {
            try
            {
                Quote res = new Quote();
                SQLiteConnection con = new SQLiteConnection("Data Source=filename.db; Version=3;");
                SQLiteCommand cmd = new SQLiteCommand(con);
                con.Open();
                cmd.CommandText = "SELECT * FROM BestQuotes ORDER BY RANDOM() LIMIT 1;";
                SQLiteDataReader reader = cmd.ExecuteReader();
                reader.Read();
                res.Text = reader["Text"].ToString();
                res.Date = reader["Date"].ToString();
                res.Number = reader["Number"].ToString();
                reader.Close();
                lastBest = "DELETE FROM BestQuotes WHERE Number='" + res.Number + "';";
                con.Close();
                return res;
            }
            catch
            {
                con.Close();
                return null;
            }
        }
        private void GetSettings()
        {
            try
            {
                using (SQLiteConnection con = new SQLiteConnection("Data Source=filename.db; Version=3;"))
                {
                    con.Open();
                    string sql = "SELECT * FROM Acomics";
                    SQLiteCommand cmd = new SQLiteCommand(sql, con);
                    SQLiteDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        _comics.Add(new Ac(reader["Num"].ToString(), reader["Name"].ToString(), reader["Album"].ToString()));
                    }
                    reader.Close();
                    cmd.CommandText = "SELECT * FROM Settings";
                    reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        lastQuote = Convert.ToInt32(reader["quote"].ToString());
                        lastItStory = Convert.ToInt32(reader["ITStrory"].ToString());
                        lastZadolbali = Convert.ToInt32(reader["ZdlblStory"].ToString());
                        lastComics = reader["comics"].ToString();
                    }
                }
            }
            catch { }
        }
        private void SaveSettings()
        {
            try
            {
                using (SQLiteConnection con = new SQLiteConnection("Data Source=filename.db; Version=3;"))
                {
                    con.Open();
                    string sql = "UPDATE Settings SET 'quote'=" + lastQuote + " ,'ITStrory'=" + lastItStory + " ,'ZdlblStory'=" + lastZadolbali + " ,'comics'=" + lastComics + ";";
                    SQLiteCommand cmd = new SQLiteCommand(sql, con);
                    cmd.ExecuteNonQuery();
                }
            }
            catch { }
        }

        private void btnSaveSettings_Click(object sender, RoutedEventArgs e)
        {
            SaveSettings();
        }
    }

}
