﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Threading;
namespace Bot.VK
{
    public class VkApi
    {
        public WebClient client = new WebClient();
        public bool Post(string id, string message, string token)
        {
            var textMsgValues = new NameValueCollection();
            textMsgValues.Add("owner_id", id);
            if (id[0] == '-')
                textMsgValues.Add("from_group", "1");
            textMsgValues.Add("message", message);
            textMsgValues.Add("access_token", token);
            byte[] res = client.UploadValues("https://api.vk.com/method/wall.post", textMsgValues);
            var str = Encoding.UTF8.GetString(res);
            if (str.Contains("error"))
                return false;
            return true;
        }
        public bool SaveWallPhoto(string id, string token, Comics comics)
        {
            return SaveWallPhoto(id, token, comics.Path, comics.Text);
        }
        public bool SaveWallPhoto(string id, string token, string path, string message = null)
        {
            JToken jToken;
            string photo, server, hash, str, attach;
            byte[] response;
            try
            {
                //получаем адрес сервера для заливки 
                bool IsGroup = id.Contains("-");

                if (IsGroup)
                    id = id.Remove(0, 1);

                var values = new NameValueCollection();

                if (IsGroup)
                    values.Add("group_id", id);
                else
                    values.Add("user_id", id);

                values.Add("access_token", token);
                response = client.UploadValues("https://api.vk.com/method/photos.getWallUploadServer", values);
                str = Encoding.Default.GetString(response);
                //парсим ответ
                jToken = JObject.Parse(str);
                str = str.Replace("\\/", "/");
                str = (string)jToken.SelectToken("response").SelectToken("upload_url");
                if (str == null)
                    return false;
                //заливаем фото
                response = client.UploadFile(str, path);
                str = Encoding.UTF8.GetString(response);
                //парсим ответ
                jToken = JObject.Parse(str);

                photo = jToken.SelectToken("photo").ToString().Replace("\r\n", "").Replace(" ", "");
                server = (string)jToken.SelectToken("server");
                hash = (string)jToken.SelectToken("hash");
                if (photo == null || hash == null || server == null)
                    return false;
                values.Clear();
                if (IsGroup)
                    values.Add("group_id", id);
                else
                    values.Add("user_id", id);
                values.Add("hash", hash);
                values.Add("server", server);
                values.Add("photo", photo);
                values.Add("access_token", token);
                //сохраняем на стену?
                response = client.UploadValues("https://api.vk.com/method/photos.saveWallPhoto", values);
                str = Encoding.Default.GetString(response).Replace("\\", "");

                jToken = JObject.Parse(str);
                jToken = jToken.SelectToken("response");
                attach = (string)jToken.First.SelectToken("id");//костыль?
                values.Clear();
                values.Add("owner_id", IsGroup ? "-" + id : id);
                values.Add("from_group", "1");
                if (message != null)
                    values.Add("message", "#bash #баш #comics@bashquote " + message);
                values.Add("attachments", attach);
                values.Add("access_token", token);

                response = client.UploadValues("https://api.vk.com/method/wall.post", values);
                str = Encoding.Default.GetString(response).Replace("\\", "");
                jToken = JObject.Parse(str);
                jToken = jToken.SelectToken("response");

            }
            catch
            { return false; }

            return true;
        }
        public List<Album> GetAlbums(string id, string token, bool needSystem = false)
        {
            List<Album> result = new List<Album>();
            Album temp = new Album();
            string str;
            byte[] response;
            JToken jToken;
            try
            {
                var values = new NameValueCollection();
                values.Add("owner_id", id);
                values.Add("need_system", needSystem ? "1" : "0");
                values.Add("access_token", token);
                response = client.UploadValues("https://api.vk.com/method/photos.getAlbums", values);
                str = Encoding.UTF8.GetString(response).Replace("\\", "");
                jToken = JObject.Parse(str);
                jToken = jToken.SelectToken("response");
                //count = (int)jToken.SelectToken("count");
                foreach (var a in JArray.Parse(jToken.ToString()))
                {
                    temp = new Album();
                    temp.id = (int)a.SelectToken("aid");
                    temp.title = (string)a.SelectToken("title");
                    result.Add(temp);
                }
            }
            catch { return null; }
            return result;
        }
        public bool SaveAlbumPhoto(string id, string aid, string token, string path, string message = null)
        {
            JToken jToken;
            string photo, server, hash, str,captcha_url,captcha_sid,captcha_key="";
            byte[] response;
            try
            {
                var values = new NameValueCollection();
                values.Add("album_id", aid);
                values.Add("group_id", id.Remove(0, 1));
                values.Add("access_token", token);
                response = client.UploadValues("https://api.vk.com/method/photos.getUploadServer", values);
                str = Encoding.Default.GetString(response);
                //парсим ответ
                jToken = JObject.Parse(str);
                str = str.Replace("\\/", "/");
                str = (string)jToken.SelectToken("response").SelectToken("upload_url");
                if (str == null)
                    return false;
                //заливаем фото
                response = client.UploadFile(str, path);
                str = Encoding.UTF8.GetString(response);
                //парсим ответ
                jToken = JObject.Parse(str);

                photo = jToken.SelectToken("photos_list").ToString().Replace("\r\n", "").Replace(" ", "");
                server = (string)jToken.SelectToken("server");
                hash = (string)jToken.SelectToken("hash");
                if (photo == null || hash == null || server == null)
                    return false;
                values.Clear();
                values.Add("album_id", aid);
                values.Add("group_id", id.Remove(0, 1));
                values.Add("hash", hash);
                values.Add("server", server);
                values.Add("photos_list", photo);
                values.Add("access_token", token);
                //сохраняем на стену?
                response = client.UploadValues("https://api.vk.com/method/photos.save", values);
                str = Encoding.Default.GetString(response).Replace("\\", "").Replace("\"[{", "[{").Replace("}]\"", "}]");
                jToken = JObject.Parse(str).SelectToken("response");
                if (message != null)
                {
                    photo = (string)jToken.First.SelectToken("pid");
                    values.Clear();
                    values.Add("owner_id", id);
                    values.Add("photo_id", photo);
                    values.Add("message", message);
                    values.Add("access_token", token);
                    values.Add("from_group", "1");
                    response = client.UploadValues("https://api.vk.com/method/photos.createComment", values);
                    str = Encoding.UTF8.GetString(response).Replace("\"[{", "[{").Replace("}]\"", "}]");
                    while(str.Contains("error"))
                    {
                        if ((string)JObject.Parse(str).SelectToken("error").SelectToken("error_code") == "14")
                        {
                            captcha_sid = (string)JObject.Parse(str).SelectToken("error").SelectToken("captcha_sid");
                            captcha_url = (string)JObject.Parse(str).SelectToken("error").SelectToken("captcha_img");
                            captcha_key = new WindowCaptcha(captcha_key,captcha_url).ShowModal();
                            values.Clear();
                            values.Add("owner_id", id);
                            values.Add("photo_id", photo);
                            values.Add("message", message);
                            values.Add("access_token", token);
                            values.Add("from_group", "1");
                            values.Add("captcha_sid", captcha_sid);
                            values.Add("captcha_key", captcha_key);
                            response = client.UploadValues("https://api.vk.com/method/photos.createComment", values);
                            str = Encoding.UTF8.GetString(response);
                        }
                    }
                    jToken = JObject.Parse(str).SelectToken("response");
                    Thread.Sleep(2000);
                }

            }
            catch { return false; }
            return true;

        }
    }

    public class Album
    {
        public int id;
        public int thumb_id;
        public int owner_id;
        public string title;
        public string description;
        public int created;
        public int updated;
        public int size;
        public bool can_upload;
        public bool upload_by_admins_only;
        public bool comments_disabled;
    }
}
