﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Bot.VK;

namespace Bot
{
    /// <summary>
    /// Логика взаимодействия для AddComics.xaml
    /// </summary>
    public partial class AddComics : Window, INotifyPropertyChanged
    {
        private Ac _ac;
        public AddComics()
        {
            _ac = new Ac();
            InitializeComponent();
            DataContext = this;
        }
        public string Num
        {
            get
            {
                return _ac.Num;
            }
            set
            {
                if(value!=_ac.Num)
                {
                    _ac.Num = value;
                    OnPropertyChanged("Num");
                }
            }
        }
        public string CName
        {
            get
            {
                return _ac.Name;
            }
            set
            {
                if (value != _ac.Name)
                {
                    _ac.Name = value;
                    OnPropertyChanged("Name");
                }
            }
        }
        public string Album
        {
            get
            {
                return _ac.Album;
            }
            set
            {
                if (value != _ac.Album)
                {
                    _ac.Album = value;
                    OnPropertyChanged("Album");
                }
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        public Ac ShowModal()
        {
            this.ShowDialog();
            return _ac;
        }

    }
}