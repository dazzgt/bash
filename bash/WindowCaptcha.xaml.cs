﻿using System.ComponentModel;
using System.Windows;

namespace Bot.VK
{
    /// <summary>
    /// Логика взаимодействия для WindowCaptcha.xaml
    /// </summary>
    public partial class WindowCaptcha : Window,INotifyPropertyChanged
    {
        private string _res;
        private string _url;
        public WindowCaptcha(string result,string url)
        {
            _res = result;
            _url = url;
            InitializeComponent();
            DataContext = this;
        }
        public string Result
        {
            get
            {
                return _res;
            }
            set
            {
                if(value!=_res)
                {
                    _res = value;
                    OnPropertyChanged("Result");
                }
            }
        }
        public string Url
        {
            get
            {
                return _url;
            }
            set
            {
                if (value != _url)
                {
                    _url = value;
                    OnPropertyChanged("Url");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        public string ShowModal()
        {
            this.ShowDialog();
            return Result;
        }

    }
}
