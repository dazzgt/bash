﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.IO;

namespace Bot.VK
{
    public class Bash
    {
        List<Quote> _quotes = new List<Quote>();
        WebClient client = new WebClient();
        public int GetNewQuotes(int last)
        {
            _quotes = new List<Quote>();
            Quote result;
            while (true)
            {
                result = GetQuote(++last);
                if (result != null)
                    _quotes.Add(result);
                else
                    return last - 1;
            }

        }

        public List<Quote> GetBestQuotes(int pageCount)
        {
            List<Quote> result = new List<Quote>();
            HtmlDocument doc = new HtmlDocument();
            string source;
            try
            {
                for (int i = 1; i <= pageCount; i++)
                {
                    source = client.DownloadString("http://bash.im/byrating/" + i.ToString());
                    doc.LoadHtml(source);
                    foreach (var a in doc.DocumentNode.SelectNodes("//div[@class=\"quote\"]"))
                    {
                        Quote quote = new Quote();
                        try
                        {
                            quote.Date = a.SelectSingleNode(".//span[@class=\"date\"]").InnerHtml;
                            quote.Number = a.SelectSingleNode(".//a[@class=\"id\"]").InnerHtml;
                            quote.Text = a.SelectSingleNode(".//div[@class=\"text\"]").InnerHtml;
                            result.Add(quote);
                        }
                        catch { }
                    }
                }

            }
            catch (Exception e)
            {
                String s = e.Message;
                return null;
            }
            return result;
        }
        public Quote GetQuote(int number)
        {
            Quote result = new Quote();
            try
            {
                string source;
                source = client.DownloadString("http://bash.im/quote/" + number.ToString());
                if (!source.Contains(number.ToString()))
                    return null;
                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(source);
                HtmlNode quote = doc.DocumentNode.SelectSingleNode("//div[@class=\"quote\"]");

                result.Date = quote.SelectSingleNode(".//span[@class=\"date\"]").InnerHtml;
                result.Number = quote.SelectSingleNode(".//a[@class=\"id\"]").InnerHtml;
                result.Text = quote.SelectSingleNode(".//div[@class=\"text\"]").InnerHtml;


            }
            catch (Exception e)
            {

                String s = e.Message;
                return null;
            }
            return result;

        }
        public Comics GetComics(string date, bool next = true)
        {
            Comics result = new Comics();
            string source;
            string Next = next?"":date;
            string motive, path;
            HtmlDocument doc = new HtmlDocument();
            HtmlNode quote;
            try
            {                
                source = client.DownloadString("http://bash.im/comics/" + date);
                doc.LoadHtml(source);
                if (next)
                {
                    foreach (var a in doc.DocumentNode.SelectNodes("//a[@class=\"arr\"]"))
                        if (a.InnerHtml.Contains("rarr"))
                            Next = a.GetAttributeValue("href","");
                    if (Next != "")
                        Next = Next.Split('/')[2];
                    else
                        return null;
                    source = client.DownloadString("http://bash.im/comics/"+Next);
                    doc.LoadHtml(source);
                }
                quote = doc.DocumentNode.SelectSingleNode("//div[@id=\"comics\"]");
                path = quote.SelectSingleNode(".//img[@id=\"cm_strip\"]").GetAttributeValue("src","");
                quote = quote.SelectSingleNode(".//span[@class=\"backlink\"]");
                if(quote.InnerHtml.Contains("quote"))
                {
                    if (quote.InnerHtml.Contains("quote"))
                    {
                        quote = quote.SelectNodes(".//a").Where(x => x.InnerHtml.Contains("цитат")).First();
                        if(quote !=null)
                        {
                                motive = quote.GetAttributeValue("href", "");
                                result.Text = GetQuote(Convert.ToInt32(motive.Split('/')[2])).GetText();
                        }
                    }
                }
                result.Path = Directory.GetCurrentDirectory()+"\\comics\\" + Next+".png";
                client.DownloadFile(path, result.Path);
                result.Date = Next;


            }
            catch{ return null; }
            return result;

        }
        public List<Quote> NewQuotes
        {
            get { return _quotes; }
        }

    }
    public class Quote
    {
        public string Number { get; set; }
        public string Date { get; set; }
        public string Text { get; set; }
        public string GetText()
        {
            string result = "";
            Text = Replacer(Text);
            result += Date + " " + Number + "\r\n" + Text;
            return result;
        }
        private string Replacer(string text)
        {
            string temp;
            temp = text.Replace("<br>", "\r\n");
            temp = temp.Replace("&quot;", "\"");
            temp = temp.Replace("@", "@ ");
            temp = temp.Replace("&lt;", "<");
            temp = temp.Replace("&gt;", ">");
            temp = temp.Replace("&prime;","'");
            return temp;
        }
    }
    public class Comics
    {
        public string Path;
        public string Text;
        public string Date;
    }
}
