﻿using Bot.VK.Properties;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace Bot.VK
{
    public class Stories
    {
        List<Story> _stories = new List<Story>();
        WebClient client = new WebClient();
        string _url;
        public Stories(string url)
        {
            _url = url;
            client.Encoding = System.Text.Encoding.UTF8;
        }

        /// <summary>
        /// Возвращает номер последней истории. Список историй содержится в NewStories
        /// </summary>
        /// <param name="last"> номер последней истории</param>
        /// <returns></returns>
        public int GetNewStories(int last)
        {
            Story result = new Story();
            _stories = new List<Story>();
            while (true)
            {
                result = GetStory(++last);
                if (result != null)
                    _stories.Add(result);
                else
                    return last - 1;
            }
        }
        public Story GetStory(int number)
        {
            Story result = new Story();
            try
            {
                var now = DateTime.Now;
                string source;
                source = client.DownloadString(_url + "/story/" + number.ToString());
                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(source);
                HtmlNode quote = doc.DocumentNode.SelectSingleNode("//div[@class=\"story\"]");

                HtmlNode meta = quote.SelectSingleNode(".//div[@class=\"meta\"]");
                result.Date = meta.SelectSingleNode(".//div[@class=\"date-time\"]").InnerHtml;
                result.Date = result.Date.Replace("Сегодня", now.Year + ":" + now.Month + ":" + now.Day);
                result.Date = result.Date.Replace("Вчера", now.Year + ":" + now.Month + ":" + (now.Day - 1).ToString());
                if (meta.InnerHtml.Contains("tags"))
                {
                    foreach (var a in meta.SelectNodes(".//a[@href]"))
                        result.Tags.Add(a.InnerHtml.Replace(" ", "_"));
                }
                result.Number = "#" + quote.SelectSingleNode(".//div[@class=\"id\"]/span").InnerHtml;
                result.Text = quote.SelectSingleNode(".//div[@class=\"text\"]").InnerHtml.Replace("<br>", "\r\n");

            }
            catch (Exception e)
            {

                String s = e.Message;
                return null;
            }
            return result;

        }
        public List<Story> NewStories
        {
            get
            {
                return _stories;
            }
        }
    }
    public class Story
    {
        public Story()
        { Tags = new List<string>(); }
        public string Number { get; set; }
        public string Date { get; set; }
        public List<string> Tags { get; set; }
        public string Text { get; set; }
        public string GetText()
        {
            string result = "";
            Text = Replacer(Text);
            if (Tags.Count != 0)
            {
                foreach (var a in Tags)
                    result += "#" + a + " ";
                result += "\n";
            }

            result += Date + " " + Number + "\r\n" + Text;
            return result;
        }
        private string Replacer(string text)
        {
            string temp;
            int tag = 0;
            temp = text.Replace("<br>", "\r\n");
            temp = temp.Replace("&quot;", "\"");
            temp = temp.Replace("&nbsp;", "");
            temp = temp.Replace("<code>", "");
            temp = temp.Replace("</code>", "");
            temp = temp.Replace("<pre>", "\r\n");
            temp = temp.Replace("</pre>", "\r\n");
            temp = temp.Replace("<p>", "\r\n");
            temp = temp.Replace("</p>", "\r\n");
            temp = temp.Replace("@", "@ ");
            temp = temp.Replace("<strong>", "");
            temp = temp.Replace("</strong>", "");
            temp = temp.Replace("<blockquote>", "");
            temp = temp.Replace("</blockquote>", "");
            temp = temp.Replace("&lt;", "<");
            temp = temp.Replace("&gt;", ">");
            temp = temp.Replace("<ul>", "");
            temp = temp.Replace("</ul>", "");
            temp = temp.Replace("<li>", "");
            temp = temp.Replace("</li>", "");
            temp = temp.Replace("<em>", "");
            temp = temp.Replace("</em>", "");
            temp = temp.Replace("<nobr>", "");
            temp = temp.Replace("</nobr>", "");
            temp = temp.Replace("&amp;", "&");
            temp = temp.Replace("<ol>", "");
            temp = temp.Replace("</ol>", "");
            temp = temp.Replace("</a>", "");
            try
            {
                do
                {
                    tag = temp.IndexOf("<a", tag);
                    if (tag >= 0)
                        temp = temp.Remove(tag, temp.IndexOf(">", tag) - tag + 1);
                } while (tag != -1);
            }
            catch { }
            return temp;
        }
    }
}
