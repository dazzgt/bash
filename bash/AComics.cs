﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace Bot.VK
{
    class AComics
    {
        static WebClient client = new WebClient();

        public static Comics GetComics(string page,string aName, bool next = true)
        {
            Comics result = new Comics();
            string source;
            string path;
            HtmlDocument doc = new HtmlDocument();
            HtmlNode comics;
            try
            {
                var val = new NameValueCollection();
                val.Add("ageRestrict", "17");
                source = client.DownloadString("http://acomics.ru/~" + aName + "/" + page);
                if (source.Contains("ageRestrict"))
                    source = Encoding.UTF8.GetString(client.UploadValues("http://acomics.ru/~" + aName + "/" + page,val));
                doc.LoadHtml(source);
                if (next)
                {
                    comics = doc.DocumentNode.SelectSingleNode("//a[@class=\"next\"]");
                    if (comics == null)
                        return null;
                    path = comics.GetAttributeValue("href", "");
                    if (path == "")
                        return null;
                    page = path.Split('/')[4];
                    source = client.DownloadString(path);
                    if (source.Contains("ageRestrict"))
                        source = Encoding.UTF8.GetString(client.UploadValues(path, val));
                    doc.LoadHtml(source);
                }
                comics = doc.DocumentNode.SelectSingleNode("//img[@id=\"mainImage\"]");
                if (comics == null)
                    return null;
                path = "http://acomics.ru/"+comics.GetAttributeValue("src", "");
                if (!Directory.Exists(Directory.GetCurrentDirectory() + "\\comics\\acomics\\" + aName + "\\"))
                    Directory.CreateDirectory(Directory.GetCurrentDirectory() + "\\comics\\acomics\\" + aName + "\\");
                result.Path = Directory.GetCurrentDirectory() + "\\comics\\acomics\\" +aName+"\\"+ page + ".png";
                client.DownloadFile(path, result.Path);
                result.Date = page;


            }
            catch { return null; }
            return result;

        }
    }
    public class Ac
    {
        public string Num { get; set; }
        public string Name { get; set; }
        public string Album { get; set; }
        public Ac()
        { }
        public Ac(string name, string album) : this("0", name, album) { }
        public Ac(string num,string name,string album)
        {
            Num = num;
            Name = name;
            Album = album;
        }

    }
}
